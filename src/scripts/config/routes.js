//import StyleguideTemplate from '../../templates/styleguide.html';
//import LoginTemplate from '../../templates/auth/login.html';
//import ForgotPasswordTemplate from '../../templates/auth/forgot-password.html';
//import ResetPasswordTemplate from '../../templates/auth/reset-password.html';

import homeTemplate from '../home/partial-home.html';
import homeListTemplate from '../home/partial-home-list.html';
import aboutTemplate from '../about/partial-about.html';
import aboutTableTemplate from '../about/table-data.html';
//import aboutController from '../about/aboutController.js';

export default ($stateProvider, $urlRouterProvider) => {

    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
    // HOME STATES AND NESTED VIEWS ========================================
    .state('home', {
        url: '/home',
        templateUrl: homeTemplate
    })

    // nested list with custom controller
    .state('home.list', {
        url: '/list',
        templateUrl: homeListTemplate,
        controller: function($scope) {
            $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
        }
    })


    // nested list with just some random string data
    .state('home.paragraph', {
        url: '/paragraph',
        template: 'I could sure use a drink right now.'
    })
        
    // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
    .state('about', {
        url: '/about',
        views: {

            // the main template will be placed here (relatively named)
            '': { templateUrl: aboutTemplate },

            // the child views will be defined here (absolutely named)
            'columnOne@about': { template: 'Look I am a column!' },

            // for column two, we'll define a separate controller 
            'columnTwo@about': { 
                templateUrl: aboutTableTemplate,
                controller: 'aboutController'
            }
        }
    });


  //$urlRouterProvider.otherwise("/login");

  //$stateProvider
    
  //  .state('styleguide', {
  //    url: "/styleguide",
  //    templateUrl: StyleguideTemplate
  //  })

  //  .state('login', {
  //    url: "/login",
  //    templateUrl: LoginTemplate
  //  })

  //  .state('forgot-password', {
  //    url: "/forgot-password",
  //    templateUrl: ForgotPasswordTemplate
  //  })

  //  .state('reset-password', {
  //    url: "/reset-password",
  //    templateUrl: ResetPasswordTemplate
  //});

}