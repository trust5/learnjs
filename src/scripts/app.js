import angular from 'angular';
import modules from './config/modules';
import routes from './config/routes';



//import AuthPanelDirective from './directives/auth-panel.js';
import aboutController from './about/aboutController.js';

var learnApp = angular.module('learnApp', modules)
	.config(routes)
    .controller('aboutController', aboutController)
    //.directive('authPanel', AuthPanelDirective)
;
