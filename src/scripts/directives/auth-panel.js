import TemplateUrl from '../../templates/_directives/auth-panel.html';

export default () => {
	return {
		restrict: 'E',
		scope: {
			color: '@',
		},
		transclude: {
			'topRightAction': '?topRightAction',
		},
		templateUrl: TemplateUrl,
	};
};