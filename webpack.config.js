var webpack = require('webpack');
var path = require('path');

module.exports = {
  context: path.join(__dirname, 'src'),
  devtool: 'source-map',
  entry: {
      scripts: './bundles/scripts.js'
      ,styles: './bundles/styles.js',
  },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',    
    filename: '[name].bundle.js',
  },
  module: {
      loaders: [
      
      { test: /\.(ttf|eot|woff2?|svg|png|jpe?g)(\?.*)?$/, loader: 'file' },


     //{
     //       test: /\.font\.(js|json)$/,
     //       loader: "style!css!fontgen"
     //},


     //{ test: /\.scss$/, loader: 'style!css!sass' },
      //{ test: /\.less/, loader: 'style!css!less' },

      { test: /\.css$/, loader: 'style!css' },

      { test: /\.html$/, loader: 'ngtemplate?relativeTo=' + path.join(__dirname, 'src') + '!html' },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ng-annotate!babel?presets[]=es2015',
      },
    ],
  },
};
